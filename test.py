import StringIO
import sys

out = StringIO.StringIO()
sys.stdout = out

import task1.py
# print something (nothing will print)


# restore stdout so we can really print (__stdout__ stores the original stdout)
sys.stdout = sys.__stdout__

# print the stored value from previous print
print out.getvalue()
