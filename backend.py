from argparse import ArgumentParser
from calculator import Number, solve
from gi import require_version
require_version('Gtk', '3.0')
require_version('Gdk', '3.0')
from gi.repository.GLib import unix_signal_add, PRIORITY_DEFAULT
from gi.repository.Gdk import Event
from gi.repository.Gtk import main, main_quit, Button, Entry, HBox, VBox, Window, STYLE_CLASS_DESTRUCTIVE_ACTION, STYLE_CLASS_SUGGESTED_ACTION
from os import environ
from os.path import exists, dirname, realpath
from setproctitle import setproctitle
from signal import SIGINT
from subprocess import check_output as sh

name = "Roman Calculator"
setproctitle(name)

class Calculator(Window):
    def __init__(self):
        # Window properties
        Window.__init__(self, title=name)
        self.set_title(name)
        self.maximize()
        self.fullscreen()
        self.set_decorated(False)
        self.set_resizable(False)
        self.set_focus()
        self.connect('destroy', main_quit)
        unix_signal_add(PRIORITY_DEFAULT, SIGINT, main_quit)

      
        # Frame 
        vframe = VBox.new(True, 10) 
        self.add(vframe)
        hframe = HBox.new(True, 10)
        vframe.pack_start(hframe, False, False, 0)

        # Calculator
        calculator = VBox.new(True, 10)
        hframe.pack_start(calculator, False, False, 0)

        # Entry
        self.entry = Entry()
        self.entry.set_alignment(1)
        self.entry.connect('activate', self.equal)
        self.entry.set_placeholder_text('Insert an expression')
        calculator.pack_start(self.entry, False, False, 0)

        # Buttons
        self.rows = [[] for i in range(5)]
        self.buttons = {}

        for s in ['(',')','^','AC']:
            button = self.create_button(s)
            self.buttons[s] = button
            self.rows[0].append(button)

        s = ['÷', '×', '-']
        N = ['N','I','II','III','IV','V','VI','VII','VIII','IX']
        for i,n in enumerate(range(1,9,3)):
            for j in range(n,n+3):
                button = self.create_button(N[j])
                self.buttons[j] = button
                self.rows[i+1].append(button)
            button = self.create_button(s[i])
            self.rows[i+1].append(button)

        for s in ['N', '.', '=', '+']:
            button = self.create_button(s)
            self.buttons[s] = button
            self.rows[4].append(button)

        for row in self.rows:
            hbox = HBox.new(True, 10)
            calculator.pack_start(hbox, False, False, 0)
            for s in row:
                hbox.pack_start(s, False, False, 0)

        # = Button
        self.buttons['AC'].get_style_context().add_class(STYLE_CLASS_DESTRUCTIVE_ACTION)
        self.buttons['='].get_style_context().add_class(STYLE_CLASS_SUGGESTED_ACTION)

    def create_button(self, label):
        button = Button(label=label)
        button.connect('clicked', self.on_button_clicked)
        return button

    def equal(self, entry):
        text = self.entry.get_text()
        result = Number(solve(text)).numeral
        self.entry.set_text(result)
 
    def on_button_clicked(self, button):
        label = button.get_label()
        same, reset, on, dot = label == "=", label == 'AC', label == '÷', label == '×'
        if same:
            self.equal(self.entry)
        if reset:
            self.entry.set_text("")
        if not same and not reset:
            if on:
                char = '/'
            if dot:
                char = '*'
            if not dot and not on:
                char = button.get_label()
            self.entry.do_insert_at_cursor(self.entry, char)
            self.entry.grab_focus_without_selecting()
 
class RomanCalculator():
    def __init__(self):
        win = Calculator()
        win.show_all()
        main()
    

if __name__ == "__main__":
    # Argument parser
    parser = ArgumentParser(description="Roman calculator")
    parser.add_argument('--verbose', dest='verbose', action='store_true', default=False, help='extended output')
    parser.add_argument('--backend', dest='backend', action='store', default='local', help='local (default), web')
    args = parser.parse_args()
    if args.verbose:
        print(args)

    if args.backend == 'web':
            executable_path = dirname(realpath(__file__))
            env = 'broadway', ':5'
            environ['GDK_BACKEND'], environ['BROADWAY_DISPLAY'] = env
            env = 'broadway', ':5'
            sh(['python', executable_path + "/" + __file__])
            del env
    RomanCalculator()
