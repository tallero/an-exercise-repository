# Start by writing a Roman Numeral converter and from that Roman Numerals. Then write a
# Roman Numeral Calculator that takes a string i.e. “XXIV + XI” and returns the result which
# would be “XXXV”. Ideally it should perform all BODMAS calculation so:
# Brackets: ()
# Order (power): ^
# Division: /
# Multiplication: *
# Addition: +
# Subtraction: -


from re import findall, sub

def solve(expr):
    expr = sub('\^', '**', expr)
    expr = sub('N', '0', expr)
    numerals = findall('[a-zA-Z]+', expr)
    for x in numerals:
        expr = sub(x, str(Number(x).integer), expr)
    return eval(expr)

class Number:
    def __init__(self, x):
        if type(x) == str:
            self.numeral = x
            self.integer = self.to_integer(x)
        if type(x) == int:
            self.integer = x
            self.numeral = self.to_numeral(x)
     
    def to_numeral(self, x):
        string = str(x)
        result = "M"*int(string[:-3]) if string[:-3] else ""
        A = (int(x) for x in string[-3:])
        R = [["C","D","M"], ["X","V","C"], ["I", "V", "X"]]
        for i,x in enumerate(A):     
            if x in range(1,4):
                result += R[i][0]*x
            if x == 4:
                result += R[i][0] + R[i][1]
            if x in range(5,9):
                result += R[i][1] + R[i][0]*(x-5)
            if x == 9:
                result += R[i][0] + R[i][2]
        return result if result != "" else "N"
    
    def to_integer(self, y):
        L = len(y)
        R = ["M","D","C","L","X","V","I"]
        A = [1000,500,100,50,10,5,1]
        result = 0
        i = 0
        get_last = True
        while i < L-1:
            P = R.index(y[i])
            p = R.index(y[i+1])
            if A[P] < A[p]:
                if P != p+2:
                    raise ValueException
                result += A[p]-A[P]
                i += 2
                get_last = False
            else:
                result += A[P]
                i += 1
                get_last = True
        if get_last:
            result += A[R.index(y[-1])]
        return result if result != 0 else "N"
