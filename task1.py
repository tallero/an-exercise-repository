# Write a program that prints the numbers from 0 to 100. But for multiples of three print
# “Fizz” instead of the number and for multiples of five prints “Buzz”. For numbers which are
# multiples of both three and five print “FizzBuzz”. Then extend the program to say
# “Flamingo” when a number is a member of the Fibonacci sequence, and “Pink Flamingo”
# when it is a multiple of 3 and 5 and a member of the Fibonacci sequence. Hint: A number is
# Fibonacci if and only if one or both of (5*n2 + 4) or (5*n2 – 4) is a perfect square (i.e. A
# number made by squaring a whole number)

from numpy import sqrt
from numpy.random import randint

def d(l,x): return (x % l) == 0
def border(x,d): return (x-d,x+d)
def fib(x): return any(sqrt(y) == int(sqrt(y)) for y in border(5*x**2,4))

def first_version(test=False):
    for x in range(100):
        string = ""
        if d(3,x):
            string = "Fizz"
        if d(5,x):
            string += "Buzz"
        if string == "":
            string = x
        print(string)

def second_version(test=False):
    for x in range(100):
        string = ""
        if d(3,x):
            string = "Fizz"
        if d(5,x):
            string += "Buzz"
        if fib(x):
            if string == "FizzBuzz":
                string = "Pink Flamingo"
            else:
                string = "Flamingo" 
        if string == "":
            string = x
        print(string)

def test(f):
import StringIO
import sys

# somewhere to store output
out = StringIO.StringIO()

# set stdout to our StringIO instance
sys.stdout = out

# print something (nothing will print)
print 'herp derp'

# restore stdout so we can really print (__stdout__ stores the original stdout)
sys.stdout = sys.__stdout__

# print the stored value from previous print
print out.getvalue()
    if f(randint*5

if __name__ == "__main__":
    # Argument parser
    parser = ArgumentParser(description="Task 1")
    parser.add_argument('--verbose', dest='verbose', action='store_true', default=False, help='extended output')
    parser.add_argument('--first-version', dest='first', action='store_true', default=False, help="execute the first version")
    parser.add_argument('--test', dest='test', action='store_true', default=False, help="check if the program is well written")

    args = parser.parse_args()

    f = first_version if args.first else second_version

    test(f) if args.test else f()
