from calculator import Number, solve
from flask import Flask, request
app = Flask(__name__)

@app.route('/solver', methods=['POST'])
def solver():
    if request.method == 'POST':
        try:
            integer = solve(request.form['expr'])
            answer = Number(integer).numeral
            return answer
        except:
            return "malformed expression"
    else:
        return """Solve numerical expressions containing roman numerals
                  
                  Input:
                      expr (str): numerical expression containing numerals
                  Returns:
                      (str) resulting expression in roman numerals"""

@app.route('/converter', methods=['POST'])
def converter():
    if request.method == 'POST':
        try:
            number = Number(request.form['number'])
            return (number.integer, number.numeral)
        except:
            return "malformed number"
    else:
        return """Convert to or from roman numerals

                  Input:
                      number (str)/(int): number in integer or roman numeral
                          format
                  Returns:
                      (int, str) couple consisting of the number in both formats"""
               
